/*
 * main.c
 *
 *  Created on: 30 sie 2018
 *      Author: kyez
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>
#include <math.h>

#include "utils/helper_functions.h"
#include "utils/LCD/lcd.h"

#define SESSION_KEY (1<<PB4)
#define ARROW_L_KEY (1<<PB2)
#define ARROW_R_KEY (1<<PB3)
#define BUFER_SET_KEY (1<<PB1)
#define PHOTOS_SET_KEY (1<<PB0)



#define SHUTTER_LED_SIM (1<<PC0);
#define ENGINE_LED_SIM (1<<PC1);

#define SHUTTER_PIN (1<<PC2);
#define FOCUS_PIN (1<<PC3);

#define ENGINE_STEP_PIN (1<<PD5)
#define ENGINE_DIR_PIN (1<<PD6)

bool bufer_set = false;
bool photos_set = false;

uint16_t bufer = 2;
uint16_t photos = 20; //zmienic na 10
uint16_t rating = 0;
uint16_t slider_length = 8250;
bool session = false;

void home_screen();
void shutter();
void engine(int steps);
void engine_down();

int main(void)
{
	TCCR0 |= (1<<WGM01);
	TCCR0 |= (1<<CS02) | (1<<CS00);

	OCR0 = 38;
	TIMSK |= (1<<OCIE0);

	DDRC = 0xFF;
	DDRD = 0xFF;
	PORTC = 0x00;
	PORTD = 0x00;

	DDRB &= ~( BUFER_SET_KEY | PHOTOS_SET_KEY| ARROW_L_KEY | ARROW_R_KEY | SESSION_KEY );
	PORTB |= BUFER_SET_KEY | PHOTOS_SET_KEY | ARROW_L_KEY | ARROW_R_KEY | SESSION_KEY;

	lcd_init( LCD_DISP_ON );

	home_screen();

	PORTC |= SHUTTER_LED_SIM;


	PORTD |= ENGINE_STEP_PIN;
	PORTD |= ENGINE_DIR_PIN;

	sei();
	while(1)
	{
		delay(80);
		if(key_pressed(SESSION_KEY) && session == false) { //uruchamianie sesji
				session = true;
				rating = round(slider_length/photos);
				delay(200);
		}

		if(session == true && bufer_set == false && photos_set == false) { //rozpoczęcie sesji zdjęciowej
			char buf[16];

			if(photos > 0) {
				lcd_clrscr();
				lcd_puts("SESSION STARTED");
				lcd_gotoxy(0,1);
				lcd_puts(itoa(photos,buf,10));

				//sygnał wyzwalacza
				shutter();
				delay(bufer * 1000);

				//rusz silnik
				engine(rating);
//				engine(1);

				delay(250); //opozneinie bezpieczenstwa na ruch silnika

				photos--;
			} else { //sesja zakonczona - wroc do ekranu startowego
				lcd_gotoxy(12,1);
				lcd_puts("DONE");

				//engine_down();

				PORTD = 0x00;

				PORTC &= ~SHUTTER_PIN;
				PORTC &= ~ENGINE_LED_SIM;

				if(key_pressed(SESSION_KEY)) {
					PORTC |= SHUTTER_LED_SIM;
					PORTC |= ENGINE_LED_SIM;

					session = false;
					home_screen();
				}
			}
		}

		if(session == false) {
			if( bufer_set == true ){
				char buf[16];

				lcd_clrscr();
				lcd_puts("Buf. time: ");
				lcd_gotoxy(0,1);
				lcd_puts(itoa(bufer,buf,10));
				lcd_puts("s");

				if(key_pressed(ARROW_R_KEY) && bufer < 60){
					bufer++;
				} else if(key_pressed(ARROW_L_KEY) && bufer > 0){
					bufer--;
				}
			}

			if( photos_set == true ){
				char buf[16];

				lcd_clrscr();
				lcd_puts("Photos num.: ");
				lcd_gotoxy(0,1);
				lcd_puts(itoa(photos,buf,10));

				if(key_pressed(ARROW_R_KEY)){
					photos = photos + 10;
				} else if(key_pressed(ARROW_L_KEY) && photos > 0){
					photos = photos - 10;
				}
			}

			if( (photos_set == false) && (bufer_set == false) ) {
				home_screen();
				delay(100);
			}
		}
	}
}

ISR(TIMER0_COMP_vect) {

static bool key_captured = false;
	//wejscie w ustawienia bufora
    if( key_pressed(BUFER_SET_KEY) )
    {
        if( !key_captured ) {
            bufer_set = !bufer_set;
            photos_set = false;
            key_captured = true;
        }
    } else if(key_pressed(PHOTOS_SET_KEY)){
    	//wejscie w ustawienia zdjec
		if( !key_captured ) {
			photos_set = !photos_set;
			bufer_set = false;
			key_captured = true;
		}
    }
    else
    {
        key_captured = false;
    }
}


void shutter() {
	//PORTC &= ~SHUTTER_LED_SIM;
	//PORTC &= ~SHUTTER_PIN;

	PORTC |= SHUTTER_PIN;
	PORTC |= FOCUS_PIN;

	delay(150);

	//PORTC |= SHUTTER_LED_SIM;

	PORTC &= ~SHUTTER_PIN;
	PORTC &= ~FOCUS_PIN;
}

void engine(int steps) {

	for(int i=0; i< steps; i++){
		PORTC &= ~ENGINE_LED_SIM;
//		PORTB |= ENGINE_DIR_PIN;
		PORTD |= ENGINE_STEP_PIN;

		delay(5);

		PORTD &= ~ENGINE_STEP_PIN;
//		PORTB &= ~ENGINE_DIR_PIN;
		PORTC |= ENGINE_LED_SIM;
		delay(5);
	}
//
//	PORTC &= ~ENGINE_LED_SIM;
//	PORTB |= ENGINE_STEP_PIN;
//
//	delay(5000);
//
//	PORTB &= ~ENGINE_STEP_PIN;
//	PORTC |= ENGINE_LED_SIM;

}

void engine_down() {
	PORTD &= ~ENGINE_DIR_PIN;

	for(int i=0; i< slider_length; i++){
		PORTC &= ~ENGINE_LED_SIM;
//		PORTB |= ENGINE_DIR_PIN;
		PORTD |= ENGINE_STEP_PIN;

		delay(2);

		PORTD &= ~ENGINE_STEP_PIN;
//		PORTB &= ~ENGINE_DIR_PIN;
		PORTC |= ENGINE_LED_SIM;
		delay(2);
	}

	PORTD |= ENGINE_DIR_PIN;
}

void home_screen(){
	char buf[16];
	lcd_clrscr();
	lcd_puts("Photly v0.2");
	lcd_gotoxy(0,1);
	lcd_puts("P=");
	lcd_puts(itoa(photos,buf,10));
	lcd_puts(" ");
	lcd_puts("T=");
	lcd_puts(itoa(bufer,buf,10));
	delay(400);
}
