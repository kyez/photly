/*
 * utils.h
 *
 *  Created on: 30 sie 2018
 *      Author: kyez
 */

#ifndef UTILS_HELPER_FUNCTIONS_H_
#define UTILS_HELPER_FUNCTIONS_H_

uint8_t key_pressed(uint8_t key);
void delay(uint16_t ms);

#endif /* UTILS_HELPER_FUNCTIONS_H_ */
