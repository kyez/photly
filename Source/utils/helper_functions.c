/*
 * utils.c
 *
 *  Created on: 30 sie 2018
 *      Author: kyez
 */

#include <avr/io.h>
#include <util/delay.h>

#include "helper_functions.h"

uint8_t key_pressed(uint8_t key) {
	if( !(PINB & key) ){
		_delay_ms(80);
		if ( !(PINB & key) ){
			return 1;
		}
	}

	return 0;
}


void delay(uint16_t ms){
	while(ms--){
		_delay_ms(1);
	}
}
